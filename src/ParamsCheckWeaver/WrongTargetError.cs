﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParamsCheckWeaver
{
    /// <summary>
    /// Stores message indicating [ParamsRequired] or [ValueRequired] attribute wrong usage on targets.
    /// Provides run-time feedback when extended rules (from AttributeTargets) are broken.
    /// </summary>
    public class WrongTargetError
    {
        /// <summary>
        /// Feedback message.
        /// </summary>
        public string Message
        {
            get
            {
                return this._message;
            }
        }
        private readonly string _message;

        /// <summary>
        /// Target FileName.
        /// </summary>
        public string FileName
        {
            get
            {
                return _fileName;
            }
        }
        private readonly string _fileName;

        /// <summary>
        /// Target LineNumber.
        /// </summary>
        public int? LineNumber
        {
            get
            {
                return _lineNumber;
            }
        }
        private readonly int? _lineNumber;


        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="message">Feedback message</param>
        /// <param name="fileName"></param>
        /// <param name="lineNumber"></param>
        public WrongTargetError(string message, string fileName, int? lineNumber)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            this._message = message;
            this._lineNumber = lineNumber;
            this._fileName = fileName;
        }
    }
}
