﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Diagnostics.SymbolStore;
using Mono.Cecil.Pdb;
using System.IO;
using System.Windows.Forms;

namespace ParamsCheckWeaver
{
    /// <summary>
    /// Reads and modifies an assembly injecting null check for reference parameters and values attributed with [ParamsRequired] or [ValueRequired] on methods, constructors and properties.
    /// Validates targets of [ParamsRequired] and [ValueRequired] attribution.
    /// Removes attributes and references to ParamsCheck assembly.
    /// </summary>
    public class Weaver
    {
        private readonly ModuleDefinition moduleDefinition;
        private readonly TypeDefinition stringType;
        private readonly TypeDefinition objectType;
        private readonly MethodDefinition argumentNullExceptionOneParamConstructor;
        private readonly MethodDefinition enumerableContainsMethod;

        /// <summary>
        /// Enumerates potential target methods of the weaver.
        /// </summary>
        private IEnumerable<MethodDefinition> TargetMethods
        {
            get
            {
                foreach (TypeDefinition typeDefinition in moduleDefinition.Types)
                {
                    //<Module> type is not user defined type
                    if (typeDefinition.Name != "<Module>")
                    {
                        foreach (MethodDefinition method in typeDefinition.Methods)
                        {
                            yield return method;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Enumerates potential target constructors of the weaver.
        /// </summary>
        private IEnumerable<MethodDefinition> TargetConstructors
        {
            get
            {
                foreach (MethodDefinition method in this.TargetMethods)
                {
                    if (method.IsConstructor)
                        yield return method;
                }
            }
        }

        /// <summary>
        /// Provides reference to ArgumentNullException(string) constructor.
        /// Imports the reference to the output assembly.
        /// </summary>
        private MethodReference ArgumentNullExceptionOneParamConstructorReference
        {
            get
            {
                if (argumentNullExceptionOneParamConstructorReference == null)
                {
                    //Import ArgumentNullException.ctor(string)
                    argumentNullExceptionOneParamConstructorReference = this.moduleDefinition.Import(argumentNullExceptionOneParamConstructor);
                }
                return argumentNullExceptionOneParamConstructorReference;
            }
        }
        private MethodReference argumentNullExceptionOneParamConstructorReference;

        /// <summary>
        /// Provides reference to <b>bool Enumerable.Contains(this IEnumerable source, object value)</b> extension method.
        /// Imports the refernece to the output assembly.
        /// </summary>
        private MethodReference EnumerableContainsReference
        {
            get
            {
                if (enumerableContainsReference == null)
                {
                    //Import System.Object
                    TypeReference objectTypeReference = this.moduleDefinition.Import(objectType);

                    //Import reference to the generic Enumerable.Contains
                    enumerableContainsReference = this.moduleDefinition.Import(enumerableContainsMethod);

                    //Create generic instance method of Enumerable.Contains
                    GenericInstanceMethod genericInstanceMethod=new GenericInstanceMethod(enumerableContainsReference);

                    //System.Object becomes the T argument type.
                    genericInstanceMethod.GenericArguments.Add(objectTypeReference);

                    //Override open type by closed type
                    enumerableContainsReference = genericInstanceMethod;
                }
                return enumerableContainsReference;
            }
        }
        private MethodReference enumerableContainsReference;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="assemblyPath">Source assembly path.</param>
        public Weaver(string assemblyPath)
        {
            DefaultAssemblyResolver pdbAssemblyResolver = new DefaultAssemblyResolver();
            pdbAssemblyResolver.AddSearchDirectory(new FileInfo(assemblyPath).Directory.FullName);

            ReaderParameters readerParameters = new ReaderParameters(ReadingMode.Immediate);
            readerParameters.ReadSymbols = true;
            readerParameters.SymbolReaderProvider = new PdbReaderProvider();
            readerParameters.AssemblyResolver = pdbAssemblyResolver;

            this.moduleDefinition = ModuleDefinition.ReadModule(assemblyPath, readerParameters);

            //Open assembly for System.* types
            AssemblyDefinition msCoreLibDefinition = moduleDefinition.AssemblyResolver.Resolve("mscorlib");

            //Open assembly for System.Core types (we need System.Linq which is in the System.Core assembly)
            AssemblyDefinition linqLibDefinition = moduleDefinition.AssemblyResolver.Resolve("System.Core");

            //Find ArgumentNullException type
            TypeDefinition argumentNullExceptionType = msCoreLibDefinition.MainModule.Types.First(t => t.Name == "ArgumentNullException");
            
            //Find System.Linq.Enumerable type
            TypeDefinition enumerableType = linqLibDefinition.MainModule.Types.First(t => t.FullName.Contains("System.Linq.Enumerable"));

            //Find String type
            stringType = msCoreLibDefinition.MainModule.Types.First(t => t.Name == "String");

            //Find Object type
            objectType = msCoreLibDefinition.MainModule.Types.First(t => t.Name == "Object");

            //Find ArgumentNullException.ctor(string)
            argumentNullExceptionOneParamConstructor = argumentNullExceptionType.Methods.First(m=>m.Parameters.Count==1 && m.Parameters[0].ParameterType==stringType);

            //Find System.Linq.Enumerable.Contains<T>(this IEnumerable<T> source,T obj)
            enumerableContainsMethod = enumerableType.Methods.First(m => m.Parameters.Count == 2 && m.FullName.Contains("Contains"));
        }

        /// <summary>
        /// Checks proper usage of [ParamsRequired] and [ValueRequired] attributes on methods, constructors, parameters and properties.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WrongTargetError> Validate()
        {
                foreach (MethodDefinition method in this.TargetMethods)
                {
                    bool requiresAllParams = method.RequiresAllParams();
                    string fileName;
                    int? lineNumber;
                    
                    method.GetSourceDocumentReference(out fileName, out lineNumber);

                    //Report error when the method is abstract ir has no body
                    if (requiresAllParams && !method.CanWeaveMethod())
                    {
                        yield return new WrongTargetError(string.Format("Method {1} on {0} is marked with [ParamsRequired] but has no body.", method.DeclaringType.FullName, method.Name),fileName,lineNumber);
                        continue;
                    }

                    //When [ParamsRequired] is placed on the method in which on reference parameter is found WrongTargetError is reported.
                    if (requiresAllParams && !method.Parameters.Any(p => p.CanCheckIfNull()))
                        yield return new WrongTargetError(string.Format("Method {1} on {0} is marked with [ParamsRequired] but there is no parameter of reference type to check.", method.DeclaringType.FullName, method.Name), fileName, lineNumber);
                    
                    foreach (ParameterDefinition parameterDefinition in method.Parameters)
                    {
                        //ValueRequired can only be attributed on reference parameters excluding OUT parameters
                        if (method.RequiresValue(parameterDefinition) && !parameterDefinition.CanCheckIfNull())
                        {
                            if (parameterDefinition.IsOut)
                                yield return new WrongTargetError(string.Format("Parameter {2} in method {1} on {0} is marked with [ValueRequired] but is out parameter.", method.DeclaringType.FullName, method.Name, parameterDefinition.Name), fileName, lineNumber);
                            else
                                yield return new WrongTargetError(string.Format("Parameter {2} in method {1} on {0} is marked with [ValueRequired] but is not of reference type.", method.DeclaringType.FullName, method.Name, parameterDefinition.Name), fileName, lineNumber);
                            
                        }

                        //Cannot attribute ValueRequired to the parameter of which owner method is alreday marked with ParamsRequired
                        if (requiresAllParams && method.RequiresValue(parameterDefinition))
                        {
                            yield return new WrongTargetError(string.Format("Parameter {2} in method {1} on {0} cannot be marked with [ValueRequired] because owner method is already marked with [ParamsRequired].", method.DeclaringType.FullName, method.Name, parameterDefinition.Name), fileName, lineNumber);
                        }
                    }
                }

                foreach (MethodDefinition method in this.TargetConstructors)
                {
                    if (!method.InitsMembers()) continue;

                    string fileName;
                    int? lineNumber;

                    method.GetSourceDocumentReference(out fileName, out lineNumber);

                    //When [InitMembers] is placed on the constructor with no parameters.
                    if (!method.Parameters.Any())
                        yield return new WrongTargetError(string.Format("Method {1} on {0} is marked with [InitMembers] but there is no parameter.", method.DeclaringType.FullName, method.Name), fileName, lineNumber);

                    foreach (ParameterDefinition parameterDefinition in method.Parameters)
                    {
                        //Parameter should have a corresponding member
                        if (!method.DeclaringType.CanAssignCorrespondingMember(parameterDefinition))
                        {
                            yield return new WrongTargetError(string.Format("Parameter {2} in constructor {1} that is marked with [InitMembers] ( type {0} ) has no corresponding member in the class", method.DeclaringType.FullName, method.Name, parameterDefinition.Name), fileName, lineNumber);
                        }
                    }
                }
        }

        /// <summary>
        /// Injects null check instructions into methods, constructors for parameters and properties attributed with [ValueRequired] or [ParamsRequired].
        /// </summary>
        public void Weave()
        {
            foreach (MethodDefinition method in this.TargetMethods)
            {
                //Method attributed with [ParamsRequired]
                bool requiresAllParams = method.RequiresAllParams();
                int instructionIndex = 0;
                    
                foreach (ParameterDefinition paramDefinition in method.Parameters)
                {
                    //If the parameter type is a reference type & is not out parameter 
                    // & the owner menthod marked with [ParamsRequired] or itself with [ValueRequired]
                    if (paramDefinition.CanCheckIfNull() && (requiresAllParams || method.RequiresValue(paramDefinition)))
                    {
                        //if no check made already for the paramDefinition
                        if (!CheckAlreadyMade(method.Body.Instructions.Skip(instructionIndex), paramDefinition))
                        {
                            foreach (Instruction nullCheckInstruction in GenerateArgumentNullException(paramDefinition))
                            {
                                method.Body.Instructions.Insert(instructionIndex++, nullCheckInstruction);
                            }
                        }
                        //parameter is a params array
                        if (paramDefinition.IsParamArray())
                        {
                            //if no check made already for the paramDefinition[]
                            if (!CheckAlreadyMade(method.Body.Instructions.Skip(instructionIndex), paramDefinition, "[]"))
                            {
                                foreach (Instruction nullCheckInstruction in GenerateArgumentItemNullException(paramDefinition))
                                {
                                    method.Body.Instructions.Insert(instructionIndex++, nullCheckInstruction);
                                }

                            }
                        }
                    } 
                }

                if (!method.InitsMembers()) continue;

                foreach (ParameterDefinition paramDefinition in method.Parameters)
                {
                    FieldDefinition fieldDefinition = method.DeclaringType.CorrespondingFields(paramDefinition).FirstOrDefault();
                    if (fieldDefinition != null)
                    {
                        foreach (Instruction fieldAssignationInstruction in GenerateFieldAssignation(fieldDefinition, paramDefinition))
                        {
                            method.Body.Instructions.Insert(instructionIndex++, fieldAssignationInstruction);
                        }
                        continue;
                    }
                    else
                    {
                        PropertyDefinition propertyDefinition = method.DeclaringType.CorrespondingProperties(paramDefinition).FirstOrDefault();
                        if (propertyDefinition != null)
                        {
                            foreach (Instruction propertyAssignationInstruction in GeneratePropertyAssignation(propertyDefinition, paramDefinition))
                            {
                                method.Body.Instructions.Insert(instructionIndex++, propertyAssignationInstruction);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Removes [ValueRequired], [ParamsRequired], [InitMembers] attributes.
        /// </summary>
        public void RemoveNullCheckAttributes()
        {
            foreach (MethodDefinition method in this.TargetMethods)
            {
                //Remove [ParamsRequired] attribute
                if (method.RequiresAllParams())
                {
                    method.RemoveParamsRequired();
                }
                //Remove [InitMembers] attribute
                if (method.InitsMembers())
                {
                    method.RemoveInitMembers();
                }
                foreach (ParameterDefinition paramDefinition in method.Parameters)
                {
                    if (method.RequiresValue(paramDefinition))
                    {
                        //Remove [ValueRequired]
                        method.RemoveRequiresValue(paramDefinition);
                    }
                }
            }
        }

        /// <summary>
        /// Removes the reference to ParamsCheck assembly form the modified result assembly.
        /// </summary>
        public void CleanReferences()
        {
            var referenceToRemove = moduleDefinition.AssemblyReferences.FirstOrDefault(x => x.Name == "ParamsCheck");
            if (referenceToRemove == null)
            {
                moduleDefinition.AssemblyReferences.Remove(referenceToRemove);
            }
        }

        /// <summary>
        /// Saves the new assembly to file.
        /// </summary>
        public void WriteOutput(string outputPath)
        {
            this.moduleDefinition.Write(outputPath);
        }

        /// <summary>
        /// Generates a collection of instructions for the equivalent code:
        /// if(parameter==null)
        ///     throw new ArgumentNullException("parameter");
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private IEnumerable<Instruction> GenerateArgumentNullException(ParameterDefinition parameter)
        {
            //Pushes the parameter on stack
            yield return Instruction.Create(OpCodes.Ldarg_S, parameter);

            //Empty instruction used to refer the place where to jump to when parameter value is not null
            Instruction notNullJump = Instruction.Create(OpCodes.Nop);

            //If value on stack is null jump to 'notNullJump'
            yield return Instruction.Create(OpCodes.Brtrue_S, notNullJump);

            //Pushes the name of 'parameter' on stack
            yield return Instruction.Create(OpCodes.Ldstr, parameter.Name);

            //Calls ArgumentNullException constructor. Last item on the stack is popped from stack and the exception is pushed.
            yield return Instruction.Create(OpCodes.Newobj, this.ArgumentNullExceptionOneParamConstructorReference);

            //Throws the object that is on stack.
            yield return Instruction.Create(OpCodes.Throw);

            //Label instruction to jump to when parameter is not NULL to skip ArgumentNullException being thrown otherwise 
            yield return notNullJump;
        }

        /// <summary>
        /// Generates a collection of instructions for the equivalent code:
        /// this.fieldName = parameter;
        /// </summary>
        private IEnumerable<Instruction> GenerateFieldAssignation(FieldDefinition fieldDefinition, ParameterDefinition parameter)
        {
            //Pushes the parameter on stack
            yield return Instruction.Create(OpCodes.Ldarg_0);

            //Pushes the parameter on stack
            yield return Instruction.Create(OpCodes.Ldarg_S, parameter);

            //assign field
            yield return Instruction.Create(OpCodes.Stfld, fieldDefinition.GetClosedFieldInstance());
        }

        /// <summary>
        /// Generates a collection of instructions for the equivalent code:
        /// this.PropertyName = parameter;
        /// </summary>
        private IEnumerable<Instruction> GeneratePropertyAssignation(PropertyDefinition propertyDefinition, ParameterDefinition parameter)
        {
            //Pushes 'this' reference on stack
            yield return Instruction.Create(OpCodes.Ldarg_0);

            //Pushes the parameter on stack
            yield return Instruction.Create(OpCodes.Ldarg_S, parameter);

            //assign property
            yield return Instruction.Create(OpCodes.Call, propertyDefinition.GetClosedSetMethodInstance());
        }

        /// <summary>
        /// Generates a collection of instructions for the equivalent code:<br/>
        /// if(paramsArray.Contains(null))<br/>
        ///     throw new ArgumentNullException("paramsArray[]");<br/>
        /// </summary>
        /// <param name="paramsArray"></param>
        /// <returns></returns>
        private IEnumerable<Instruction> GenerateArgumentItemNullException(ParameterDefinition paramsArray)
        {
            //Pushes the param array on stack
            yield return Instruction.Create(OpCodes.Ldarg_S, paramsArray);

            //Pushes NULL value on stack
            yield return Instruction.Create(OpCodes.Ldnull);

            //Calls paramsArray.Contains(null) and pushes the result on stack 
            yield return Instruction.Create(OpCodes.Call, this.EnumerableContainsReference);

            //Empty instruction used to refer the place where to jump to when paramsArray does not contain NULL
            Instruction doesNotContainNullJump = Instruction.Create(OpCodes.Nop);

            //If value on stack is false jump to 'doesNotContainNullJump'
            yield return Instruction.Create(OpCodes.Brfalse_S, doesNotContainNullJump);

            //Pushes the paramsArray name on stack
            yield return Instruction.Create(OpCodes.Ldstr, paramsArray.Name + "[]");

            //Calls ArgumentNullException constructor. Last item on the stack is popped from stack and the exception is pushed.
            yield return Instruction.Create(OpCodes.Newobj, this.ArgumentNullExceptionOneParamConstructorReference);

            //Throws the object that is on stack.
            yield return Instruction.Create(OpCodes.Throw);

            //Empty label instruction to jump to when all elements of paramsArray are diffrent from NULL to skip ArgumentNullException being thrown otherwise
            yield return doesNotContainNullJump;
        }

        /// <summary>
        /// Checks if a chain of instructions contains equivalent of ArgumentNullException being thrown for a null parameter.
        /// </summary>
        /// <param name="instructions">A chain of instructions</param>
        /// <param name="parameter">ParameterDefinition</param>
        /// <param name="postfix">Postfix to the parameter name to match the ArgumentNullException argument.</param>
        /// <returns></returns>
        private bool CheckAlreadyMade(IEnumerable<Instruction> instructions, ParameterDefinition parameter, string postfix="")
        {
            Func<Instruction, bool>[] argumentNullExceptionThrownInstructionsPredicates = new Func<Instruction, bool>[] 
            { 
                //parameter name pushed on stack ?
                i=>i.OpCode==OpCodes.Ldstr && (i.Operand.ToString()==parameter.Name+postfix),
                
                //ArgumentNullException created ?
                i=>i.OpCode==OpCodes.Newobj&&i.Operand.ToString()==ArgumentNullExceptionOneParamConstructorReference.ToString(),
                
                //object on stack thrown ?
                i=>i.OpCode==OpCodes.Throw
            };

            int nullCheckInstructionIndex=0;
            foreach (Instruction instruction in instructions)
            {
                if (argumentNullExceptionThrownInstructionsPredicates[nullCheckInstructionIndex](instruction))
                {
                    if(++nullCheckInstructionIndex==argumentNullExceptionThrownInstructionsPredicates.Length)
                        //all instructions matched
                        return true;
                }
                else
                {
                    //instruction match failed
                    nullCheckInstructionIndex = 0;
                }
            }
            return false;
        }
    }
}
