﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace ParamsCheckWeaver
{
    /// <summary>
    /// ParamsCheck Weaver extensions.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Indicates if the method has the [ParamsRequired] attribute on it.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        public static bool RequiresAllParams(this MethodDefinition methodDefinition)
        {
            return methodDefinition.CustomAttributes.GetAttribute(typeof(ParamsCheck.ParamsRequiredAttribute))!=null;
        }

        /// <summary>
        /// Indicates if the constructor has the [InitMembers] attribute on it.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        public static bool InitsMembers(this MethodDefinition methodDefinition)
        {
            return methodDefinition.CustomAttributes.GetAttribute(typeof(ParamsCheck.InitMembersAttribute)) != null;
        }

        /// <summary>
        /// Removes ParamsRequired attribute from the method.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        public static void RemoveParamsRequired(this MethodDefinition methodDefinition)
        {
            if(methodDefinition.RequiresAllParams())
                methodDefinition.CustomAttributes.Remove(methodDefinition.CustomAttributes.GetAttribute(typeof(ParamsCheck.ParamsRequiredAttribute)));
        }

        /// <summary>
        /// Removes [InitMembers] attribute from the method.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        public static void RemoveInitMembers(this MethodDefinition methodDefinition)
        {
            if (methodDefinition.InitsMembers())
                methodDefinition.CustomAttributes.Remove(methodDefinition.CustomAttributes.GetAttribute(typeof(ParamsCheck.InitMembersAttribute)));
        }

        /// <summary>
        /// Indicates if the parameter has the [ValueRequired] attribute on it.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        public static bool RequiresValue(this MethodDefinition methodDefinition, ParameterDefinition parameterDefinition)
        {
            if(methodDefinition.IsSetter)
            {
                if (methodDefinition.DeclaringType.Properties.First(p => p.SetMethod == methodDefinition).CustomAttributes.GetAttribute(typeof(ParamsCheck.ValueRequiredAttribute)) != null)
                {
                    return true;
                }
            }            
            return parameterDefinition.CustomAttributes.GetAttribute(typeof(ParamsCheck.ValueRequiredAttribute)) != null;
        }

        /// <summary>
        /// Indicates if the parameter can be succesfully checked for null.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        /// <returns></returns>
        public static bool CanCheckIfNull(this ParameterDefinition parameterDefinition)
        {
            if (parameterDefinition.IsOut)
                return false;
            if (!parameterDefinition.ParameterType.IsGenericParameter)
                return !parameterDefinition.ParameterType.IsValueType;
            else
                return ((GenericParameter)parameterDefinition.ParameterType).HasReferenceTypeConstraint();
        }

        public static MethodReference GetClosedSetMethodInstance(this PropertyDefinition propertyDefinition)
        {
            GenericInstanceType type = new GenericInstanceType(propertyDefinition.DeclaringType);
            foreach (var genericParam in propertyDefinition.DeclaringType.GenericParameters)
                type.GenericArguments.Add(genericParam);
            MethodReference refern = new MethodReference(propertyDefinition.SetMethod.Name, propertyDefinition.SetMethod.ReturnType, type);
            refern.Parameters.Add(propertyDefinition.SetMethod.Parameters[0]);
            refern.HasThis = true;
            return refern;
        }

        public static FieldReference GetClosedFieldInstance(this FieldDefinition fieldDefinition)
        {
            GenericInstanceType type = new GenericInstanceType(fieldDefinition.DeclaringType);
            foreach (var genericParam in fieldDefinition.DeclaringType.GenericParameters)
                type.GenericArguments.Add(genericParam);
            return new FieldReference(fieldDefinition.Name, fieldDefinition.FieldType, type);
        }
        
        public static bool CanAssignCorrespondingMember(this TypeDefinition typeDefinition, ParameterDefinition parameterDefinition)
        {
            if (parameterDefinition.IsOut)
                return false;
            return typeDefinition.CorrespondingFields(parameterDefinition).Cast<object>().Concat(typeDefinition.CorrespondingProperties(parameterDefinition)).Any();
        }

        public static IEnumerable<FieldDefinition> CorrespondingFields(this TypeDefinition typeDefinition, ParameterDefinition parameterDefinition)
        {
            foreach(var field in typeDefinition.Fields)
            {
                if (field.FieldType.Resolve() == parameterDefinition.ParameterType.Resolve())
                {
                    if(field.Name == parameterDefinition.Name)
                    {
                        yield return field;
                    }
                    else if(field.Name.ToLower() == parameterDefinition.Name.ToLower())
                    {
                        yield return field;
                    }
                    else if (field.Name.ToLower() == "_" + parameterDefinition.Name.ToLower())
                    {
                        yield return field;
                    }
                }
            }
        }

        public static IEnumerable<PropertyDefinition> CorrespondingProperties(this TypeDefinition typeDefinition, ParameterDefinition parameterDefinition)
        {
            foreach (var property in typeDefinition.Properties)
            {
                if (property.PropertyType.Resolve() == parameterDefinition.ParameterType.Resolve() || property.SetMethod != null)
                {
                    if (property.Name == parameterDefinition.Name)
                    {
                        yield return property;
                    }
                    else if (property.Name.ToLower() == parameterDefinition.Name.ToLower())
                    {
                        yield return property;
                    }
                    else if ("_" + property.Name.ToLower() == parameterDefinition.Name.ToLower())
                    {
                        yield return property;
                    }
                }
            }
        }

        /// <summary>
        /// Check if the GenericParameter is restricted to be of refernece type or extends reference type.
        /// </summary>
        /// <param name="genericParameter">GenericParameter</param>
        /// <returns></returns>
        public static bool HasReferenceTypeConstraint(this GenericParameter genericParameter)
        {
            if (genericParameter.Attributes.HasFlag(GenericParameterAttributes.NotNullableValueTypeConstraint) || genericParameter.Constraints.Any(t => !t.IsReferenceType()))
                return false;
            return genericParameter.Attributes.HasFlag(GenericParameterAttributes.ReferenceTypeConstraint) || genericParameter.Constraints.Any(t => t.IsReferenceType());
        }

        /// <summary>
        /// Indicates if the type limites generic to reference.
        /// </summary>
        /// <param name="typeReference"></param>
        /// <returns></returns>
        public static bool IsReferenceType(this TypeReference typeReference)
        {
            if (typeReference.IsValueType)
                return false;
            if (typeReference.FullName == "System.ValueType")
                return false;
            return true;
        }
        /// <summary>
        /// Checks if the method is not abstract and has body.
        /// </summary>
        /// <param name="methodDefinition"></param>
        /// <returns></returns>
        public static bool CanWeaveMethod(this MethodDefinition methodDefinition)
        {
            return !methodDefinition.IsAbstract && methodDefinition.HasBody;
        }

        /// <summary>
        /// Indicates if the parameter has the [ValueRequired] attribute on it.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        public static void RemoveRequiresValue(this MethodDefinition methodDefinition, ParameterDefinition parameterDefinition)
        {
            if (methodDefinition.RequiresValue(parameterDefinition))
            {
                if (methodDefinition.IsSetter)
                {
                    PropertyDefinition property=methodDefinition.DeclaringType.Properties.First(p => p.SetMethod == methodDefinition);
                    property.CustomAttributes.Remove(property.CustomAttributes.GetAttribute(typeof(ParamsCheck.ValueRequiredAttribute)));
                    return;
                }
                parameterDefinition.CustomAttributes.Remove(parameterDefinition.CustomAttributes.GetAttribute(typeof(ParamsCheck.ValueRequiredAttribute)));
            }
        }
        /// <summary>
        /// Indicates if the parameter is a params array.
        /// </summary>
        /// <param name="parameterDefinition"></param>
        /// <returns></returns>
        public static bool IsParamArray(this ParameterDefinition parameterDefinition)
        {
            return parameterDefinition.CustomAttributes.GetAttribute(typeof(ParamArrayAttribute))!=null;
        }

        /// <summary>
        /// Looks up the CustomAttribute.
        /// </summary>
        /// <param name="attributes">CustomAttributes</param>
        /// <param name="type">Attribute type</param>
        /// <returns>CustomAttribute or null if not found.</returns>
        public static CustomAttribute GetAttribute(this IEnumerable<CustomAttribute> attributes, Type type)
        {
            return attributes.FirstOrDefault(attribute => attribute.Constructor.DeclaringType.FullName == type.FullName);
        }

        /// <summary>
        /// Finds the file name and line number where the method is defined.
        /// </summary>
        /// <param name="methodDefinition"></param>
        /// <param name="fileName"></param>
        /// <param name="lineNumber"></param>
        public static void GetSourceDocumentReference(this MethodDefinition methodDefinition, out string fileName, out int? lineNumber )
        {
            if(methodDefinition.HasBody)
            {
                Instruction instruction = methodDefinition.Body.Instructions.FirstOrDefault();
                if (instruction != null && instruction.SequencePoint != null && instruction.SequencePoint.Document != null && instruction.SequencePoint.Document.Url!=null)
                {
                    fileName = instruction.SequencePoint.Document.Url;
                    lineNumber = instruction.SequencePoint.StartLine;
                    return;
                }
            }
            fileName = null;
            lineNumber = new int?();
        }
    }
}
