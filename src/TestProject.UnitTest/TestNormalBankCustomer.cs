﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace TestProject.UnitTest
{
    [TestClass]
    public class TestNormalBankCustomer
    {
        #region Test NormalBankCustomer(string surname,string names)
       
        [TestMethod]
        public void Test_Constructor_surname_ArgumentNullException()
        {
            string surname = null;
            Invoking(() => new NormalBankCustomer(surname, "aaa bbb")).ShouldThrow<ArgumentNullException>();
        }

        private static Action Invoking(Action action)
        {
            return action;
        }

        [TestMethod]
        public void Test_Constructor_names_ArgumentNullException()
        {
            string names=null;
            Invoking(() => new NormalBankCustomer("Moss", names)).ShouldThrow<ArgumentNullException>();
        }

        #endregion

        #region Test NormalBankCustomer(string surname, RestrictedAccount account, [ValueRequired] params string[] names)

        [TestMethod]
        public void Test_Constructor_surename_ArgumentNullException()
        {
            string[] names = new string[] { };
            Invoking(() => new NormalBankCustomer(null, new RestrictedAccount(), names)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Constructor_account_ThrowsArgumentNullException()
        {
            RestrictedAccount account = null;
            string[] names = new string[] { };
            Invoking(() => new NormalBankCustomer("Moss", account, names)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Constructor_namesArray_ArgumentNullException()
        {
            string[] names = null;
            Invoking(() => new NormalBankCustomer("Moss", new RestrictedAccount(), names)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Constructor_namesArray_Null_Check_Passed()
        {
            string[] names = new string[]{};
            Invoking(() => new NormalBankCustomer("Moss", new RestrictedAccount(), names)).ShouldNotThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Constructor_namesArrayElement_ArgumentNullException()
        {
            string[] names = new[] { "", null };
            Invoking(() => new NormalBankCustomer("Moss", new RestrictedAccount(), names)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Constructor_namesArrayElements_Null_Check_Passed()
        {
            string[] names = new[] { "", "" };
            Invoking(() => new NormalBankCustomer("Moss", new RestrictedAccount(), names)).ShouldNotThrow();
        }

        #endregion

        [TestMethod]
        public void Test_Constructor_Null_Check_Passed()
        {
            Invoking(() => new NormalBankCustomer("Moss", "aaa bbb")).ShouldNotThrow();
        }

        #region Test NormalBankCustomer(string surname, RestrictedAccount account, [ValueRequired] params string[] names)

        [TestMethod]
        public void Test_Constructor_account_ArgumentNullException()
        {
            Invoking(() => new NormalBankCustomer(null)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Constructor_account_Null_Check_Passed()
        {
            RestrictedAccount account = new RestrictedAccount();
            Invoking(() => new NormalBankCustomer(account)).ShouldNotThrow();
        }

        #endregion

        #region Test Add
        [TestMethod]
        public void Test_Add_Method_comment_ArgumentNullException()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => normalBankCustomer.Add(0, null)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Add_Method_comment_Null_Check_Passed()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => normalBankCustomer.Add(0, "ignore")).ShouldNotThrow();
        }
        #endregion

        #region Test Charge

        [TestMethod]
        public void Test_Charge_Method_comment_Null_Check_Passed()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            string proofKey = null;
            Invoking(() => normalBankCustomer.Charge(0, "ignore", out proofKey)).ShouldNotThrow();
        }
        [TestMethod]
        public void Test_Charge_Method_comment_ArgumentNullException()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            string proofKey = null;
            Invoking(() => normalBankCustomer.Charge(0, null, out proofKey)).ShouldThrow<ArgumentNullException>();
        }

        #endregion

        #region Test Account setter

        [TestMethod]
        public void Test_Set_Account_ArgumentNullException()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => normalBankCustomer.Account = null).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Set_Account_Null_Check_Passed()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => normalBankCustomer.Account = new RestrictedAccount()).ShouldNotThrow();
        }

        #endregion

        #region Test SuspendCustomers
        
        [TestMethod]
        public void Test_Static_SuspendCustomers_Element_ArgumentNullException()
        {
            Invoking(() => BankCustomer<Account<decimal>, decimal>.SuspendCustomers(null,null)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Static_SuspendCustomers_ArgumentNullException()
        {
            BankCustomer<Account<decimal>, decimal>[] toSuspend = null;
            Invoking(() => BankCustomer<Account<decimal>, decimal>.SuspendCustomers(toSuspend)).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Static_SuspendCustomers_Elements_Passed()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => NormalBankCustomer.SuspendCustomers(normalBankCustomer, normalBankCustomer)).ShouldNotThrow();
        }
        
        #endregion


        [TestMethod]
        public void Test_Indexer_Value_ArgumentNullException()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => normalBankCustomer[""] = null).ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Indexer_Parameter_ArgumentNullException()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => normalBankCustomer[null] = "").ShouldThrow<ArgumentNullException>();
        }

        [TestMethod]
        public void Test_Indexer_Null_Check_Passed()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account);
            Invoking(() => normalBankCustomer[""] = "").ShouldNotThrow();
        }

        [TestMethod]
        public void Test_InitMembers()
        {
            RestrictedAccount account = new RestrictedAccount();
            NormalBankCustomer normalBankCustomer = new NormalBankCustomer(account,"names");
            normalBankCustomer.Account.ShouldBeEquivalentTo(account);
            normalBankCustomer._customerName.ShouldBeEquivalentTo("names");
        }
    }
}
