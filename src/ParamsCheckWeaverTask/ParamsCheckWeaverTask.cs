﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Utilities;
using ParamsCheckWeaver;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Build.Framework;

/// <summary>
/// Weaves null checks.
/// </summary>
public class ParamsCheckWeaverTask : Task
{
    [Required]
    public string AssemblyPath { get; set; }

    /// <summary>
    /// Weaves null checks.
    /// </summary>
    /// <returns></returns>
    public override bool Execute()
    {
        if (!File.Exists(AssemblyPath))
        {
            this.Log.LogError("File " + this.AssemblyPath + " does not exist");
            return false;
        }

        Weaver weaver = null;

        try
        {
            weaver = new Weaver(this.AssemblyPath);
        }
        catch(Exception exception)
        {
            this.Log.LogError("Error while opening " + this.AssemblyPath + ". " + exception.Message);
            return false;
        }

        try
        {
            bool invalid = false;
            foreach (WrongTargetError error in weaver.Validate())
            {
                if (error.FileName != null && error.LineNumber.HasValue)
                {
                    
                    this.Log.LogError("Target Error", "1", "Attribute", error.FileName, error.LineNumber.Value, 0, error.LineNumber.Value, 0, error.Message);
                }
                else
                {
                    this.Log.LogError(error.Message);
                }
                invalid = true;
            }
            if(invalid)
                return false;
        }
        catch (Exception exception)
        {
            this.Log.LogError("Error while validating " + this.AssemblyPath + ". " + exception.Message);
            return false;
        }

        try
        {
            weaver.Weave();
        }
        catch (Exception exception)
        {
            this.Log.LogError("Error while weaving " + this.AssemblyPath + ". " + exception.Message);
            return false;
        }

        try
        {
            weaver.RemoveNullCheckAttributes();
        }
        catch (Exception exception)
        {
            this.Log.LogError("Error while removing check attributes " + this.AssemblyPath + ". " + exception.Message);
            return false;
        }

        try
        {
            weaver.WriteOutput(AssemblyPath+"~");
        }
        catch (Exception exception)
        {
            this.Log.LogError("Error while saving assemblly. " + exception.Message);
            return false;
        }

        try
        {
            File.Delete(this.AssemblyPath);
            File.Copy(AssemblyPath + "~", AssemblyPath);
            File.Delete(AssemblyPath + "~");
        }
        catch (Exception exception)
        {
            this.Log.LogError(string.Format("Cannot override {0} assembly ( new assembly was saved to {0}~ ). ", this.AssemblyPath) + exception.Message);
            return false;
        }
        return true;
    }
}
