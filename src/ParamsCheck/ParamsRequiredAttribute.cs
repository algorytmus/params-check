﻿using System;

namespace ParamsCheck
{
    /// <summary>
    /// Injects null checks for all parameters of reference type in a method or constructor.
    /// </summary>
    /// <remarks>Can only be used on methods and constructors with at least one reference parameter.</remarks>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor, AllowMultiple = false, Inherited = false)]
    public class ParamsRequiredAttribute : Attribute
    {
    }
}
