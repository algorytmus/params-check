﻿using System;

namespace ParamsCheck
{
    /// <summary>
    /// Injects a null check for the reference parameter in a method, setter or constructor body.
    /// If attributed to constructor parameters null checks are injected before calling base constructors.
    /// </summary>
    /// <remarks>Can only be used on reference parameters (in methods and constructors) and properties.</remarks>
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValueRequiredAttribute : Attribute
    {

    }
}
