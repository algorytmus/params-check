﻿using System;

namespace ParamsCheck
{
    /// <summary>
    /// Injects field and property assignation from constructor parameters.
    /// </summary>
    /// <remarks>Can only be used on constructors with at least one parameter.</remarks>
    [AttributeUsage(AttributeTargets.Constructor, AllowMultiple = false, Inherited = false)]
    public class InitMembersAttribute : Attribute
    {

    }
}
