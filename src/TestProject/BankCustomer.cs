﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParamsCheck;

namespace TestProject
{
    public abstract class BankCustomer<TAccount,TMoney> where TMoney : struct where TAccount : Account<TMoney>
    {
        public readonly string _customerName;

        [ValueRequired]
        public TAccount Account
        {
            get;
            set;
        }

        [ParamsRequired]
        protected BankCustomer(TAccount account, string surname, params string[] names)
        {
            this.Account = account;
            this._customerName = surname + string.Join(" ", names);
        }

        [ParamsRequired]
        [InitMembers]
        public BankCustomer(TAccount account, string customerName)
        {
            this.Account = account;
        }

        public abstract void Charge(TMoney amount, string comment, out string proof);


        [ParamsRequired]
        public void Add(TMoney amount, string comment)
        {
            this.Account.TransferMoney(amount);
        }

        public static void SuspendCustomers( [ValueRequired] params BankCustomer<TAccount, TMoney>[] names)
        {

        }
    }
}
