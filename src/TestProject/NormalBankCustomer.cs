﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParamsCheck;

namespace TestProject
{
    
    public class NormalBankCustomer : BankCustomer<RestrictedAccount,decimal>
    {
        public NormalBankCustomer(string surname, RestrictedAccount account, [ValueRequired] params string[] names)
            :base(account,surname,names.Skip(1).ToArray())
        {
        }

        public NormalBankCustomer(RestrictedAccount account, string customerName) : base(account, customerName) { }


        [ParamsRequired]
        public NormalBankCustomer(string surname,string names)
            : base(new RestrictedAccount(), surname.ToUpper(), names.Split(' '))
        {
        }

        public NormalBankCustomer([ValueRequired] RestrictedAccount restrictedAccount)
            : base(new RestrictedAccount(), string.Empty)
        {
        }
        [ValueRequired]
        public string this[string something]
        {
            set
            {
                something.ToLower();
            }
        }

        [ParamsRequired]
        public override void Charge(decimal amount, string comment, out string proof)
        {
            if (Account.CanSubtrack(amount))
                Account.SubstractMoney(amount);
            else
                throw new InvalidOperationException("The customer is not liable to pay. Cannot charge for " + comment);
            proof = "key";
        }
    }
}
